# Connect4DX

Connect4 on GameBoy with visuals !

![Connect4 main menu](title_screen.png) ![Connect4 player 2 wins](game_screen.png)

## Play
- Download ROM: connect4dx.gb
- Load the ROM in your favorite GameBoy emulator!

## Build
### Setup
- Install SDCC and add it to your PATH
- Compile (or get a compiled version of) GBDK-n
- Move all GBDK-n folder inside this project's folder
  - For instance, `gbdk-n-compile.sh` should be at path `connect4/gbdk-n/gbdk-n-compile.sh`

### Compile
- From your favorite terminal, go inside this project's folder
- Type `make`
