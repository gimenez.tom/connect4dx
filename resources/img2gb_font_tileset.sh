#!/bin/sh

img2gb tileset \
	--output-c-file=../src/font_tileset.c \
    --output-header-file=../src/font_tileset.h \
    --output-image=font_tileset.png \
    --deduplicate \
    ./font.png