#!/bin/sh

img2gb tileset \
	--output-c-file=../src/tileset.c \
    --output-header-file=../src/tileset.h \
    --output-image=tileset.png \
    --deduplicate \
    ./tileset_from_gimp.png