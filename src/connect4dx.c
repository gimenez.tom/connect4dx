#include <stdlib.h>
//#include <stdio.h>
#include <gb/gb.h>
//#include <gb/console.h>

#include "font_helpers.h"

#include "tileset.h"
#include "tilemap.h"

#define CURSOR_DRAW_OFFSET_X 6
#define CURSOR_DRAW_OFFSET_Y 3
#define GRID_DRAW_OFFSET_X 6
#define GRID_DRAW_OFFSET_Y 5
#define GRID_FRAME_LEFT_COLUMN_DRAW_OFFSET_X 5
#define GRID_FRAME_RIGHT_COLUMN_DRAW_OFFSET_X 13
#define GRID_FRAME_BOTTOM_LINE_DRAW_OFFSET_Y 11
#define RESULT_OFFSET_X 3
#define RESULT_DRAW_OFFSET_X 7
#define RESULT_OFFSET_Y 15

#define GRID_WIDTH 7
#define GRID_HEIGHT 6

#define COLUMN_FULL 0x3F // 0b00111111

#define GAME_COIN_P1 1
#define GAME_COIN_P2 2

#define GAME_RESULT_UNKNOWN 0
#define GAME_RESULT_P1_WINS 1
#define GAME_RESULT_DRAW 3

#define ARROW_DOWN_KEY 0x2

UINT8* GRID_P1;
UINT8* GRID_P2;

UINT8* DIAGONALS_LENGTHS;

UINT8 GAME_PLAYER_TURN;
UINT8 GAME_CURSOR_X;
UINT8 GAME_RESULT;

// Title
void TitleScene();

void TitleScene()
{
	set_bkg_tiles(0, 0, TILEMAP_WIDTH, TILEMAP_HEIGHT, TILEMAP);

	SHOW_BKG;

	//gotoxy(5, 8);
	//printf("Connect 4");
	text_print_string_bkg(5, 3, "Connect 4");

	text_print_string_bkg(2, 15, "- Press START -");

	waitpad(J_START);
	waitpadup();
}

// Game
void InitGrids();
void ClearCursor();
void DrawCursor();
void GameDrawGridFrame();
void DrawResult();
UINT8 CanAddCoin();
void AddCoin();
void ChangePlayerTurn();
void GameHandleJoypad();
UINT8 GameEvaluateFullness();
UINT8 GameEvaluateColums();
UINT8 GameEvaluateRows();
UINT8 GameEvaluateDiagonals();
void GameEvaluateGrid();
void GameUpdate();
void GameReset();
void GameScene();

void InitGrids()
{
	for (UINT8 i = 0; i != GRID_WIDTH; ++i)
	{
		GRID_P1[i] = 0;
		GRID_P2[i] = 0;
	}
}

void ClearCursor()
{
	UINT8 emptyCursor[] = {0, 0};
	set_bkg_tiles(
		CURSOR_DRAW_OFFSET_X + GAME_CURSOR_X, CURSOR_DRAW_OFFSET_Y,
		1, 2,
		emptyCursor
		);
	//setchar(' ');
	//gotoxy(CURSOR_DRAW_OFFSET_X + GAME_CURSOR_X, CURSOR_DRAW_OFFSET_Y + 1);
	//setchar(' ');
}

void DrawCursor()
{
	// Draw current player's coin and arrow under it.
	UINT8 cursor[] = {0, 3};
	if (GAME_PLAYER_TURN == 0)
	{
		cursor[0] = 1;
	}
	else
	{
		cursor[0] = 2;
	}
	set_bkg_tiles(
		CURSOR_DRAW_OFFSET_X + GAME_CURSOR_X, CURSOR_DRAW_OFFSET_Y,
		1, 2,
		cursor
		);

	// Draw arrow down.
	//gotoxy(CURSOR_DRAW_OFFSET_X + GAME_CURSOR_X, CURSOR_DRAW_OFFSET_Y + 1);
	//setchar(ARROW_DOWN_KEY);
}

void GameDrawGridFrame()
{
	set_bkg_tiles(0, 0, TILEMAP_WIDTH, TILEMAP_HEIGHT, TILEMAP);
}

void DrawResult()
{
	if (GAME_RESULT == GAME_RESULT_DRAW)
	{
		text_print_string_bkg(RESULT_DRAW_OFFSET_X, RESULT_OFFSET_Y, "Draw!");
	}
	else
	{
		UINT8 playerCoin; 
		if (GAME_RESULT == GAME_RESULT_P1_WINS)
		{
			text_print_string_bkg(RESULT_OFFSET_X, RESULT_OFFSET_Y, "Player 1 wins!");
			playerCoin = GAME_COIN_P1;
		}
		else
		{
			text_print_string_bkg(RESULT_OFFSET_X, RESULT_OFFSET_Y, "Player 2 wins!");
			playerCoin = GAME_COIN_P2;
		}
	}
}

UINT8 CanAddCoin()
{
	// Current player can add coin if the column isn't full yet.
	UINT8 column = GRID_P1[GAME_CURSOR_X] | GRID_P2[GAME_CURSOR_X];
	return column != COLUMN_FULL;
}

void AddCoin()
{
	// Compute column usage.
	UINT8 column = GRID_P1[GAME_CURSOR_X] | GRID_P2[GAME_CURSOR_X];

	UINT8* playerColumn;
	UINT8 playerCoin;
	if (GAME_PLAYER_TURN == 0)
	{
		playerColumn = &GRID_P1[GAME_CURSOR_X];
		playerCoin = GAME_COIN_P1;
	}
	else
	{
		playerColumn = &GRID_P2[GAME_CURSOR_X];
		playerCoin = GAME_COIN_P2;
	}

	// Compute row index by searching first empty spot in the column.
	UINT8 rowIndex = 0;
	while (column & (1 << rowIndex)) ++rowIndex;

	// Set this spot as occupied.
	*playerColumn += (1 << rowIndex);

	// Draw the coin.
	set_bkg_tiles(
		GRID_DRAW_OFFSET_X + GAME_CURSOR_X, (GRID_DRAW_OFFSET_Y + GRID_HEIGHT) - (rowIndex + 1),
		1, 1,
		&playerCoin
		);
}

void ChangePlayerTurn()
{
	if (GAME_PLAYER_TURN == 0)
	{
		GAME_PLAYER_TURN = 1;
	}
	else
	{
		GAME_PLAYER_TURN = 0;
	}	
}

void GameHandleJoypad()
{
	if (GAME_RESULT == GAME_RESULT_UNKNOWN)
	{
		UINT8 keys = waitpad(J_LEFT | J_RIGHT | J_A | J_START);

		if ((keys & J_LEFT) && GAME_CURSOR_X != 0)
		{
			ClearCursor();
			--GAME_CURSOR_X;
			DrawCursor();
		}

		if ((keys & J_RIGHT) && GAME_CURSOR_X != GRID_WIDTH - 1)
		{
			ClearCursor();
			++GAME_CURSOR_X;
			DrawCursor();
		}	

		if ((keys & J_A) && CanAddCoin())
		{
			AddCoin();
			GameEvaluateGrid();
			if (GAME_RESULT == GAME_RESULT_UNKNOWN)
			{
				ChangePlayerTurn();
				ClearCursor();
				DrawCursor();
			}
		}

		if (keys & J_START)
		{
			GameReset();
		}	
	}
	else
	{
		UINT8 keys = waitpad(J_START);

		if (keys & J_START)
		{
			GameReset();
		}
	}

	waitpadup();
}

UINT8 GameEvaluateFullness()
{
	for (UINT8 i = 0; i != GRID_WIDTH; ++i)
	{
		UINT8 column = GRID_P1[i] | GRID_P2[i];
		if (column != COLUMN_FULL)
		{
			return 0;
		}
	}

	return 1;
}

UINT8 GameEvaluateColums()
{
	UINT8* grid;
	if (GAME_PLAYER_TURN == 0)
	{
		grid = GRID_P1;
	}
	else
	{
		grid = GRID_P2;
	}

	for (UINT8 i = 0; i != GRID_WIDTH; ++i)
	{
		const UINT8 column = grid[i];
		UINT8 count = 0;
		for (UINT8 j = 0; j != GRID_HEIGHT; ++j)
		{
			const UINT8 onSpot = column & (1 << j);
			if (onSpot)
			{
				++count;
				if (count == 4)
				{
					return GAME_PLAYER_TURN + 1;
				}
			}
			else
			{
				count = 0;
			}
		}
	}

	return 0;
}

UINT8 GameEvaluateRows()
{
	UINT8* grid;
	if (GAME_PLAYER_TURN == 0)
	{
		grid = GRID_P1;
	}
	else
	{
		grid = GRID_P2;
	}	

	for (UINT8 i = 0; i != GRID_HEIGHT; ++i)
	{
		UINT8 count = 0;
		for (UINT8 j = 0; j != GRID_WIDTH; ++j)
		{
			const UINT8 column = grid[j];
			const UINT8 onSpot = column & (1 << i);
			if (onSpot)
			{
				++count;
				if (count == 4)
				{
					return GAME_PLAYER_TURN + 1;
				}
			}
			else
			{
				count = 0;
			}	
		}
	}

	return 0;
}

UINT8 GameEvaluateDiagonals()
{
	UINT8* grid;
	if (GAME_PLAYER_TURN == 0)
	{
		grid = GRID_P1;
	}
	else
	{
		grid = GRID_P2;
	}	

	// For each left diagonal.
	for (UINT8 diagonalIndex = 0; diagonalIndex != 4; ++diagonalIndex)
	{
		const UINT8 startingColumnIndex = 3 + diagonalIndex;
		UINT8 count = 0;
		// Get its length.
		const UINT8 length = DIAGONALS_LENGTHS[diagonalIndex];
		// Go up diagonally.
		for (UINT8 i = 0; i != length; ++i)
		{
			// Get current column.
			const UINT8 column = grid[startingColumnIndex - i];
			// At current row, check if player has put a coin.
			const UINT8 onSpot = column & (1 << i);
			if (onSpot)
			{
				++count;
				if (count == 4)
				{
					return GAME_PLAYER_TURN + 1;
				}
			}
			else
			{
				count = 0;
			}	
		}	
	}

	// For each right diagonal.
	for (UINT8 diagonalIndex = 0; diagonalIndex != 4; ++diagonalIndex)
	{
		const UINT8 startingColumnIndex = 3 - diagonalIndex;
		UINT8 count = 0;
		// Get its length.
		const UINT8 length = DIAGONALS_LENGTHS[diagonalIndex];
		// Go up diagonally.
		for (UINT8 i = 0; i != length; ++i)
		{
			// Get current column.
			const UINT8 column = grid[startingColumnIndex + i];
			// At current row, check if player has put a coin.
			const UINT8 onSpot = column & (1 << i);
			if (onSpot)
			{
				++count;
				if (count == 4)
				{
					return GAME_PLAYER_TURN + 1;
				}
			}
			else
			{
				count = 0;
			}	
		}	
	}

	return 0;
}

void GameEvaluateGrid()
{
	// Evaluate columns.
	UINT8 result = GameEvaluateColums();
	if (result)
	{
		GAME_RESULT = result;
		return;
	}

	// Evaluate rows.
	 result = GameEvaluateRows();
	if (result)
	{
		GAME_RESULT = result;
		return;
	}
	// Evaluate diagonals.
	result = GameEvaluateDiagonals();
	if (result)
	{
		GAME_RESULT = result;
		return;
	}

	// Evaluate "fullness".
	if (GameEvaluateFullness())
	{
		GAME_RESULT = GAME_RESULT_DRAW;
		return;
	}
}

void GameUpdate()
{
	// Handle joypad.
	GameHandleJoypad();

	// Display result if game is over.
	if (GAME_RESULT != GAME_RESULT_UNKNOWN)
	{
		DrawResult();
	}	
}

void GameReset()
{
	InitGrids();
	GAME_CURSOR_X = GRID_WIDTH / 2;
	GAME_PLAYER_TURN = 0;
	GAME_RESULT = GAME_RESULT_UNKNOWN;

	GameDrawGridFrame();
	DrawCursor();
}

void GameScene()
{
	GameReset();

	while (1)
	{
		GameUpdate();
	}	
}

/****************
*****************
****************/

void Setup()
{
	// Initialize static variables.
	GRID_P1 = malloc(GRID_WIDTH * sizeof(UINT8));
	GRID_P2 = malloc(GRID_WIDTH * sizeof(UINT8));

	DIAGONALS_LENGTHS = malloc(4 * sizeof(UINT8));
	DIAGONALS_LENGTHS[0] = 4;
	DIAGONALS_LENGTHS[1] = 5;
	DIAGONALS_LENGTHS[2] = 6;
	DIAGONALS_LENGTHS[3] = 6;

	// Load background's tileset.
	set_bkg_data(0, TILESET_TILE_COUNT, TILESET);
	// Load background's font tileset.
	text_load_font();

	// Load scene Title.
	TitleScene();
}

void Loop()
{
	GameScene();
}

void main(void)
{
	Setup();
	while(1)
	{
		Loop();
	}
}

